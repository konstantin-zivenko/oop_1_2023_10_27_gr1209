# Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
# назву, рік видання та жанрі. Створіть кілька книжок. Визначте для нього
# операції перевірки на рівність та нерівність, методи __repr__ та __str__.


class Book:
    def __init__(
        self,
        title: str,
        published: int,
        genre: str,
        author: str,
    ):
        self.title = title
        self.author = author
        self.published = published
        self.genre = genre

    def __str__(self):
        return f"book: {self.title}, author: {self.author}"

    def __repr__(self):
        return (
            f"Book({self.title!r}, {self.author!r}, {self.published!r}, {self.genre!r})"
        )

    def __eq__(self, other):
        if not type(other) == Book:
            raise TypeError(f"expected Book-type, but got {type(other)}")
        return self.title == other.title and self.author == other.author

    def __ne__(self, other):
        if not type(other) == Book:
            raise TypeError(f"expected Book-type, but got {type(other)}")
        return self.title != other.title or self.author != other.author


if __name__ == "__main__":
    book_1 = Book("Foundation", 2010, "scifi", "Isaac Azimov")

    book_2 = Book("Inferno", 2020, "mystic", "Den Braun")

    print(book_1 == book_2)
